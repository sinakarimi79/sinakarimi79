<h2 align="left">Hi <img align="left"src="https://gitlab.com/sinakarimi79/sinakarimi79/-/raw/main/Assets/Hi.gif?inline=false" width="29px"  />!</h2>


<img align="right" height="150" width="250" src="https://gitlab.com/sinakarimi79/sinakarimi79/-/raw/main/Assets/programmerPhoto.jpg?inline=false"  />


###

<div align="left">
    <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/unity/unity-original.svg" height="40" width="52" alt="unity logo"  />
    <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-original.svg" height="30" width="42" alt="csharp logo"  />
</div>

###

<div align="left">
  <img src="https://img.shields.io/static/v1?message=Instagram&logo=instagram&label=&color=E4405F&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="instagram logo"  />
  <img src="https://img.shields.io/static/v1?message=Discord&logo=discord&label=&color=7289DA&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="discord logo"  />
  <a href = "https://mail.google.com/mail/u/?authuser=sinakarimi0938@gmail.com"> <img src="https://img.shields.io/static/v1?message=Gmail&logo=gmail&label=&color=D14836&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="gmail logo"  /> </a>
  <a href = "https://www.linkedin.com/in/sina-karimi-a13a28170/"> <img src="https://img.shields.io/static/v1?message=LinkedIn&logo=linkedin&label=&color=0077B5&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="linkedin logo"  /> </a>
</div>